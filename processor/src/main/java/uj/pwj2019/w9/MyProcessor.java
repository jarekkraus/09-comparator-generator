package uj.pwj2019.w9;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@SupportedSourceVersion(SourceVersion.RELEASE_11)
@SupportedAnnotationTypes({"uj.pwj2019.w9.MyComparable"})
public class MyProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            annotatedElements.forEach(this::processElement);
        }
        return true;
    }

    private void processElement(Element e) {
        TypeElement clazz = (TypeElement) e;
        String className = clazz.getQualifiedName().toString();
        var fields = e.getEnclosedElements()
                .stream()
                .filter(element -> element.getKind().isField()
                        && !element.getModifiers().contains(Modifier.PRIVATE)
                        && element.asType().getKind().isPrimitive())
                .map(element -> new HashMap.SimpleEntry<>(element,
                        element.getAnnotation(ComparePriority.class))).collect(
                        Collectors.toList());

        var sortedFields = fields.stream()
                .filter(pair -> pair.getValue() != null)
                .sorted(Comparator.comparingInt(element -> element.getValue().value()))
                .collect(Collectors.toList());
        sortedFields.addAll(
                fields.stream()
                        .filter(element -> element.getValue() == null)
                        .collect(Collectors.toList())
        );
        try {
            JavaFileObject file = processingEnv.getFiler().createSourceFile(className + "Comparator");
            String packageName = packageName(className);

            try (PrintWriter out = new PrintWriter(file.openWriter())) {
                //Package
                if (packageName != null) {
                    out.println("package " + packageName + ";");
                }
                //Class
                out.println("class " + clazz.getSimpleName() + "Comparator {");
                out.println("\tint compare(" + e.getSimpleName() + " f1, " + e.getSimpleName() + " f2) {");
                for (var field : sortedFields) {
                    var typeKind = field.getKey().asType().getKind();
                    switch (typeKind){
                        case BOOLEAN:
                            out.println(comparisonBoolean(field.getKey().getSimpleName().toString()));
                            break;
                        default:
                            out.println(comparisonDefault(field.getKey().getSimpleName().toString()));
                    }
                }
                out.println("\t\treturn 0;");
                out.println("\t}");

                out.print("}");

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String comparisonBoolean(String field) {
        StringBuilder tmp = new StringBuilder();
        tmp.append(
                "\t\tif(f1." + field + "== true && f2." + field + " == false) return 1;\n " +
                "\t\tif(f1." + field + "== false && f2." + field + " == true) return -1;"
        );
        return tmp.toString();
    }

    private String comparisonDefault(String field) {
        StringBuilder tmp = new StringBuilder();
        tmp.append(
                "\t\tif(f1." + field + "<f2." + field + ") return -1;\n " +
                "\t\tif(f1." + field + ">f2." + field + ") return 1;"
        );
        return tmp.toString();
    }

    private String packageName(String className) {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }
        return packageName;
    }
}
